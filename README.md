# ForecastApp

## Explantion:

### As user I want to see my history search.

- Solution for this problem is save the string to UserDefault on search bar when the user tap on search button in keyboard.
- Other way to solve this is saving it to database.

### Provide the UX/UI searching/history search forecast screen.

- When open app user will see the weather forecast screen with center text ask user to search city to see its weather forecast by tapping on magnifier icon on top right corner.
- After tap magnifier icon, user will see the search city screen. On appear, it will focus on to the search bar with list of history search text below and cancel button on the right.
- User enter the city name they want to know its forecast to search bar and tap search on keyboard or choose the history search text, app will call API and show the result below the search bar.
- When user choose the city in list it will turn back to weather forecast screen and begin call API to get the weather forecast.
- When the API return response, the screen will show city name, current date time and temperatue. The list below is presented the forecast for the next 7 days with date time, icon weather status, max and min temperature.

### Caching the result of the search to prevent requests to the server many times.

- The solution I used for this problem is using global variable cache in CacheManager so i can save and load cache.
- When the API search for city request return success I will save data it to cache with key is the search text. 
- When user using the same search text as before i will load data from cache then display it on UI rather than request API again.
- About the weather API, I do not use cache here because I have get the current value from API response which will difference by time so I need to request it whenever it change. 
- But if I do not use the current value, I think I will save the daily forecast response to cache with Time stamp value in order to check if that forecast is out of date or not.

### If the network is not available, the application will auto request the query right after the network back.

- I have used Alamofire.NetworkReachabilityManager() to check the network connection.
- I have created NetworkReconnectDelegate in order to do task based on which ViewController is appeared.
- On AppDelegate I have call the function startNetworkReachabilityObserver() to observe the network all time. 
- You can see these functions and values in NetworkManager class.

### Setup project able to build with 3 environment development, staging, production.

- In this problem I have 2 solution for it:
- First, I created config files for 3 environment and save all the API key, API URL. (You can see it on origin/master)
- Second, I saved all config API key, API URL in the User-Defined (You can see it on branch origin/config_build_setting)
- Both solution I used EnvironmentManager class to get value for key.
- I have chose the first method for config build because I think it would be better for encrypt.

### Encrypt your configuration to prevent getting the server configuration(like appId).

- I do not have solution for this althrough I have research for this problem.
