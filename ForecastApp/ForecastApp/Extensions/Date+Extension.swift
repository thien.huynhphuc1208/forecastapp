//
//  Date+Extension.swift
//  ForecastApp
//
//  Created by Thiện Huỳnh on 22/01/2021.
//

import Foundation

extension Date {
    func stringFromDate(with patern: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = patern
        return dateFormatter.string(from: self)
    }
}
