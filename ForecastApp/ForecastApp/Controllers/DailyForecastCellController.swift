//
//  DailyForecastCellController.swift
//  ForecastApp
//
//  Created by Thiện Huỳnh on 22/01/2021.
//

import UIKit

class DailyForecastCellController: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblMaxTemp: UILabel!
    @IBOutlet weak var lblMinTemp: UILabel!
    @IBOutlet weak var imgViewWeatherStatus: UIImageView!
    
    var model: DailyForecast?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateView(model: DailyForecast) {
        self.model = model
        if let dt = model.dt, let weatherElement = model.weather?.first, let icon = weatherElement.icon, let temp = model.temp, let maxTemp = temp.max, let minTemp = temp.min {
            self.lblDate.text = Date(timeIntervalSince1970: dt).stringFromDate(with: "dd/MM/yyyy")
            self.lblMaxTemp.text = "\(maxTemp)\u{00B0}C"
            self.lblMinTemp.text = "\(minTemp)\u{00B0}C"
            self.imgViewWeatherStatus.image = UIImage(named: "ic_\(icon)")
        }
    }
}
