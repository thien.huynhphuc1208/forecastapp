//
//  ViewController.swift
//  ForecastApp
//
//  Created by Thiện Huỳnh on 19/01/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class CitySearchViewController: UIViewController {
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableViewCitySearch: UITableView!
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var lblNoResult: UILabel!
    
    var cities: [CityModel] = [] {
        didSet {
            self.tableViewCitySearch.reloadData()
        }
    }
    var searchHistory: [String] = []
    var onSearch = false
    var delegate: CityForecastDelegate?
    let network = NetworkManager.shared
    let cellId = "citiCell"
    let historyUserDefaultKey = "history_search"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.searchBar.becomeFirstResponder()
        
        self.searchBar.delegate = self
        
        self.tableViewCitySearch.delegate = self
        self.tableViewCitySearch.dataSource = self
        
        if let list =  UserDefaults.standard.value(forKey: self.historyUserDefaultKey) as? [String] {
            self.searchHistory = list
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.network.delegate = self
    }
    
    func fetchingData(cityName: String) {
        self.onSearch = true
        self.viewLoading.isHidden = false
        self.tableViewCitySearch.isHidden = true
        self.lblNoResult.text = "Validating city....."
        self.getCitySearchResults(city: cityName) { (results) in
            if results.count == 0 {
                self.lblNoResult.text = "No city found"
            } else {
                self.viewLoading.isHidden = true
                self.tableViewCitySearch.isHidden = false
                self.cities = results
            }
        } onError: { (error) in
            self.onSearch = false
            if !self.network.isConnected {
                self.lblNoResult.text = "No internet connection"
            } else {
                self.lblNoResult.text = "\((error as NSError).localizedDescription)"
            }
        }
    }
     
    func getCitySearchResults(city: String, completion: @escaping ((_ result: [CityModel]) -> Void), onError: @escaping ((_ error: Error) -> Void)) {
        
        if let results = CacheManager.cache.object(forKey: city as NSString) as? [CityModel] {
            completion(results)
        } else {
            AF.request(EnvironmentManager.geoUrl, parameters: ["key": EnvironmentManager.geoApiKey, "q": city, "limit": 100]).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    do {
                        if let results = json["results"].arrayObject as? [[String: Any]] {
                            let resultsData = try JSONSerialization.data(withJSONObject: results)
                            let decodedResults = try JSONDecoder().decode([CityModel].self, from: resultsData)
                            let filterResults = decodedResults.filter({$0.components?._category == "place"})
                            CacheManager.cache.setObject(filterResults as NSArray, forKey: city as NSString)
                            completion(filterResults)
                        }
                    } catch let error {
                        onError(error)
                    }
                case .failure(let error):
                    onError(error)
                }
            }
        }
    }
}

extension CitySearchViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.onSearch = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !self.searchHistory.contains(searchBar.text ?? "") {
            self.searchHistory.append(searchBar.text ?? "")
        }
        UserDefaults.standard.setValue(self.searchHistory, forKey: self.historyUserDefaultKey)
        self.fetchingData(cityName: searchBar.text ?? "")
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.onSearch = false
            self.tableViewCitySearch.reloadData()
        }
    }
}

extension CitySearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.onSearch ? self.cities.count : self.searchHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath) as? CityCellController else { return UITableViewCell() }
        cell.lblTitle?.text = self.onSearch ? self.cities[indexPath.row].formatted : self.searchHistory[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if onSearch {
            if !self.searchHistory.contains(self.searchBar.text ?? "") {
                self.searchHistory.append(self.searchBar.text ?? "")
            }
            UserDefaults.standard.setValue(self.searchHistory, forKey: self.historyUserDefaultKey)
            self.delegate?.fetchingCityForecast(city: self.cities[indexPath.row])
            self.navigationController?.popViewController(animated: true)
        } else {
            self.searchBar.text = self.searchHistory[indexPath.row]
            self.fetchingData(cityName: self.searchHistory[indexPath.row])
            self.tableViewCitySearch.reloadData()
        }
    }
}

extension CitySearchViewController: NetworkReconnectDelegate {
    func handleOnReconnection() {
        if let searchText = searchBar.text, !searchText.isEmpty {
            self.fetchingData(cityName: searchText)
        } else {
            self.onSearch = false
            self.viewLoading.isHidden = true
            self.tableViewCitySearch.isHidden = false
            self.tableViewCitySearch.reloadData()
        }
    }
    
    
}
