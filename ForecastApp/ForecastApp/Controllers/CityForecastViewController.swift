//
//  CityForecastViewController.swift
//  ForecastApp
//
//  Created by Thiện Huỳnh on 21/01/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol CityForecastDelegate {
    func fetchingCityForecast(city: CityModel)
}

class CityForecastViewController: UIViewController {
    
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblCurrentTime: UILabel!
    @IBOutlet weak var lblTemperature: UILabel!
    @IBOutlet weak var tableViewDailyForecast: UITableView!
    @IBOutlet weak var viewEmpty: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    
    let cellId = "dailyForecastId"
    let network = NetworkManager.shared
    var city: CityModel?
    var forecasts: [DailyForecast] = [] {
        didSet {
            self.tableViewDailyForecast.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewDailyForecast.delegate = self
        self.tableViewDailyForecast.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.network.delegate = self
    }
    
    @IBAction func didTapSearchCity(_ sender: Any) {
        guard let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "citySearchViewController") as? CitySearchViewController else { return }
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getCitiWeeklyForecast(lat: Double, lng: Double, success: @escaping ((_ results: WeatherModel) -> Void), onError: @escaping ((_ errorMSg: String) -> Void)) {
        AF.request(EnvironmentManager.weatherUrl, parameters: ["appid": EnvironmentManager.weatherApiKey, "lat": lat, "lon": lng]).responseJSON { (response) in
            switch response.result {
            case .success(_):
                do {
                    if let data = response.data {
                        let dailyForcasts = try JSONDecoder().decode(WeatherModel.self, from: data)
                        success(dailyForcasts)
                    } else {
                        onError("data nil")
                    }
                } catch let error {
                    onError((error as NSError).localizedDescription)
                }
            case .failure(let error):
                if self.network.isConnected {
                    onError(error.localizedDescription)
                } else {
                    onError("No Internet Connection")
                }
                
            }
        }
    }
    
}

extension CityForecastViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.forecasts.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath) as? DailyForecastCellController else { return UITableViewCell() }
        cell.updateView(model: self.forecasts[indexPath.row + 1])
        return cell
    }
    
    
}

extension CityForecastViewController: CityForecastDelegate {
    func fetchingCityForecast(city: CityModel) {
        guard let lat = city.geometry?.lat, let lng = city.geometry?.lng else { return }
        self.city = city
        self.lblMessage.text = "On fetching ........"
        self.getCitiWeeklyForecast(lat: lat, lng: lng) { (results) in
            if let currentResult = results.current, let dt = currentResult.dt, let temp = currentResult.temp, let daily = results.daily {
                
                if daily.count == 0 {
                    self.lblMessage.text = "No results found!!!"
                } else {
                    self.viewEmpty.isHidden = true
                    self.forecasts = daily
                    self.lblCityName.text = city.components?.city ?? city.formatted
                    self.lblCurrentTime.text = Date(timeIntervalSince1970: dt).stringFromDate(with: "dd/MM/yyyy HH:mm")
                    self.lblTemperature.text = "\(temp)\u{00B0}C"
                }
            } else {
                self.lblMessage.text = "No results found!!!"
            }
        } onError: { (errorMsg) in
            self.viewEmpty.isHidden = false
            self.lblMessage.text = errorMsg
        }
        
    }
}

extension CityForecastViewController: NetworkReconnectDelegate {
    func handleOnReconnection() {
        if let city = self.city {
            self.fetchingCityForecast(city: city)
        } else {
            self.lblMessage.text = "Please search for city to get the forecast"
        }
    }
}
