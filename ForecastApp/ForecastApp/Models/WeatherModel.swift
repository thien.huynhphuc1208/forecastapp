//
//  WeatherModel.swift
//  ForecastApp
//
//  Created by Thiện Huỳnh on 21/01/2021.
//

import Foundation

class WeatherModel: Codable {
    var current: CurrentForecast?
    var daily: [DailyForecast]?

    init(current: CurrentForecast?, daily: [DailyForecast]?) {
        self.daily = daily
        self.current = current
    }
}

class CurrentForecast: Codable {
    var dt: TimeInterval?
    var sunrise: TimeInterval?
    var sunset: TimeInterval?
    var temp: Double?
    var pressure: Int?
    var humidity: Int?
    var dewPoint: Double?
    var uvi: Double?
    var clouds: Int?
    var visibility: Int?
    var windSpeed: Double?
    var windDeg: Int?
    var weather: [WeatherElement]?
    var pop: Double?
    
    init(dt: TimeInterval?, sunrise: TimeInterval?, sunset: TimeInterval?, temp: Double?, pressure: Int?, humidity: Int?, dewPoint: Double?, uvi: Double?, clouds: Int?, visibility: Int?, windSpeed: Double?, windDeg: Int?, weather: [WeatherElement]?, pop: Double?) {
        self.dt = dt
        self.sunrise = sunrise
        self.sunset = sunset
        self.temp = temp
        self.pressure = pressure
        self.humidity = humidity
        self.dewPoint = dewPoint
        self.uvi = uvi
        self.clouds = clouds
        self.visibility = visibility
        self.windSpeed = windSpeed
        self.windDeg = windDeg
        self.weather = weather
        self.pop = pop
    }
}

// MARK: - Daily
class DailyForecast: Codable {
    var sunrise: TimeInterval?
    var dewPoint: Double?
    var pop: Double?
    var windDeg: Int?
    var pressure: Int?
    var weather: [WeatherElement]?
    var clouds: Int?
    var windSpeed: Double?
    var dt: TimeInterval?
    var humidity: Int?
    var temp: Temp?
    var sunset: TimeInterval?
    var uvi: Double?
    var rain: Double?
    
    init(sunrise: TimeInterval?, dewPoint: Double?, pop: Double?, windDeg: Int?, pressure: Int?, weather: [WeatherElement]?, clouds: Int?, windSpeed: Double?, dt: TimeInterval?, humidity: Int?, temp: Temp?, sunset: TimeInterval?, uvi: Double?, rain: Double?) {
        self.sunrise = sunrise
        self.dewPoint = dewPoint
        self.pop = pop
        self.windDeg = windDeg
        self.pressure = pressure
        self.weather = weather
        self.clouds = clouds
        self.windSpeed = windSpeed
        self.dt = dt
        self.humidity = humidity
        self.temp = temp
        self.sunset = sunset
        self.uvi = uvi
        self.rain = rain
    }
}

class Temp: Codable {
    var day: Double?
    var max: Double?
    var night: Double?
    var morn: Double?
    var eve: Double?
    var min: Double?
    
    init(day: Double?, max: Double?, night: Double?, morn: Double?, eve: Double?, min: Double?) {
        self.day = day
        self.max = max
        self.night = night
        self.morn = morn
        self.eve = eve
        self.min = min
    }
}

// MARK: - WeatherElement
class WeatherElement: Codable {
    var id: Int?
    var main: String?
    var weatherDescription: String?
    var icon: String?
    
    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription
        case icon
    }
    
    init(id: Int?, main: String?, weatherDescription: String?, icon: String?) {
        self.id = id
        self.main = main
        self.weatherDescription = weatherDescription
        self.icon = icon
    }
}
