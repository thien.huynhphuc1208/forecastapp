//
//  CityModel.swift
//  ForecastApp
//
//  Created by Thiện Huỳnh on 20/01/2021.
//

import Foundation

// MARK: - Result
class CityModel: Codable {
    var components: Components?
    var confidence: Int?
    var formatted: String?
    var geometry: Geometry?
    
    init(components: Components?, confidence: Int?, formatted: String?, geometry: Geometry?) {
        self.components = components
        self.confidence = confidence
        self.formatted = formatted
        self.geometry = geometry
    }
}


// MARK: - Geometry
class Geometry: Codable {
    var lat: Double?
    var lng: Double?
    
    init(lat: Double?, lng: Double?) {
        self.lat = lat
        self.lng = lng
    }
}

// MARK: - Components
class Components: Codable {
    var _category: String?
    var _type: String?
    var city: String?
    var continent: String?
    var country: String?
    var countryCode: String?
    
    init(category: String?, type: String?, city: String?, continent: String?, country: String?, countryCode: String?) {
        self._category = category
        self._type = type
        self.city = city
        self.continent = continent
        self.country = country
        self.countryCode = countryCode
    }
}
