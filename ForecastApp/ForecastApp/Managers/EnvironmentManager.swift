//
//  Environment.swift
//  ForecastApp
//
//  Created by Thiện Huỳnh on 20/01/2021.
//

import Foundation

class EnvironmentManager {
    enum Keys {
        static let geoApiKey = "GEO_API_KEY"
        static let weatherApiKey = "WEATHER_API_KEY"
        static let geoUrlKey = "GEO_URL"
        static let weatherUrlKey = "WEATHER_URL"
    }
    
    private static let infoDicts: [String: Any] = {
        guard let infoDicts = Bundle.main.infoDictionary else { fatalError("plist not found!!!!") }
        return infoDicts
    }()
    
    static let geoApiKey: String = {
        guard let apiKey = infoDicts[Keys.geoApiKey] as? String else { fatalError("geo api key not found!!!") }
        return apiKey.replacingOccurrences(of: "\\", with: "").replacingOccurrences(of: "\"", with: "")
    }()
    
    static let weatherApiKey: String = {
        guard let apiKey = infoDicts[Keys.weatherApiKey] as? String else { fatalError("weather api key not found!!!") }
        return apiKey.replacingOccurrences(of: "\\", with: "").replacingOccurrences(of: "\"", with: "")
    }()
    
    static let geoUrl: String = {
        guard let url = infoDicts[Keys.geoUrlKey] as? String else { fatalError("geo url not found!!!") }
        return url.replacingOccurrences(of: "\\", with: "").replacingOccurrences(of: "\"", with: "")
    }()
    
    static let weatherUrl: String = {
        guard let url = infoDicts[Keys.weatherUrlKey] as? String else { fatalError("weather url key not found!!!") }
        return url.replacingOccurrences(of: "\\", with: "").replacingOccurrences(of: "\"", with: "")
    }()
}
