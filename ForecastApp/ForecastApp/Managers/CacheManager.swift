//
//  CacheManager.swift
//  ForecastApp
//
//  Created by Thiện Huỳnh on 22/01/2021.
//

import Foundation

class CacheManager {
    static let cache = NSCache<NSString, NSArray>()
}
