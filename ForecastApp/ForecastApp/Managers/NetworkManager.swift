//
//  NetworkManager.swift
//  ForecastApp
//
//  Created by Thiện Huỳnh on 20/01/2021.
//

import Foundation
import Alamofire

class NetworkManager {
    static let shared = NetworkManager()
    
    private let reachabilityManager = Alamofire.NetworkReachabilityManager()
    
    var isConnected: Bool = false
    var delegate: NetworkReconnectDelegate?
    
    func startNetworkReachabilityObserver() {
        
        reachabilityManager?.startListening(onUpdatePerforming: { [weak self] (status) in
            guard let weakSelf = self else { fatalError("can not convert nil") }
            switch status {
            case .notReachable:
                weakSelf.isConnected = false
                print("The network is not reachable")
            case .unknown :
                weakSelf.isConnected = false
                print("It is unknown whether the network is reachable")
            case .reachable(.ethernetOrWiFi), .reachable(.cellular):
                print("The network is reachable")
                if !weakSelf.isConnected {
                    weakSelf.isConnected = true
                    weakSelf.delegate?.handleOnReconnection()
                }
            }
        })
    }
}

protocol NetworkReconnectDelegate {
    func handleOnReconnection()
}
